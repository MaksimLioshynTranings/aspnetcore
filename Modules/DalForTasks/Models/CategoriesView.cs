﻿using Microsoft.AspNetCore.Http;

namespace DalForTasks.Models
{
    public class CategoriesView
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public IFormFile Image { get; set; }
        public byte[] Picture { get; set; }
    }
}