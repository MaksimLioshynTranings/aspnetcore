﻿using AspNetCore.Http.Extensions;
using DalForTasks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HttpClientTask4
{
    class Program
    {
        static readonly HttpClient Client = new HttpClient();

        static void ShowProduct(Products product)
        {
            Console.WriteLine($"ProductId: {product.ProductId,3}\t Name: {product.ProductName,30}\t Price: {product.UnitPrice,8}$");
        }

        static async Task<Uri> CreateProductAsync(Products product)
        {
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/products", product);

            //var dataAsString = JsonConvert.SerializeObject(product);
            //var content = new StringContent(dataAsString);
            //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            //var a = Client.PostAsync("api/products", content);
            //OR
            //var b = Client.PostAsync("api/products", new StringContent(dataAsString, Encoding.UTF8, "application/json"));

            response.EnsureSuccessStatusCode();
            return response.Headers.Location;
        }

        static async Task<Products> GetProductAsync(string path)
        {
            Products product = null;
            HttpResponseMessage response = await Client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                product = await response.Content.ReadAsJsonAsync<Products>();
            }
            return product;
        }

        static async void GetAllProductAsync()
        {
            HttpResponseMessage response = await Client.GetAsync("api/products/");
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                List<Products> list = JsonConvert.DeserializeObject<List<Products>>(jsonString);
                foreach (Products product in list)
                {
                    ShowProduct(product);
                }
            }

        }

        static async Task<Products> UpdateProductAsync(Products product)
        {
            HttpResponseMessage response = await Client.PutAsJsonAsync($"api/products/{product.ProductId}", product);
            response.EnsureSuccessStatusCode();

            product = await response.Content.ReadAsJsonAsync<Products>();
            return product;
        }

        static async Task<HttpStatusCode> DeleteProductAsync(string id)
        {
            HttpResponseMessage response = await Client.DeleteAsync($"api/products/{id}");
            return response.StatusCode;
        }

        static void Main()
        {
            RunAsync().GetAwaiter().GetResult();
        }

        static async Task RunAsync()
        {
            Client.BaseAddress = new Uri("https://localhost:44366/");
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                Products product = new Products
                {
                    ProductName = "testName",
                    QuantityPerUnit = "1000",
                    UnitPrice = (decimal?) 18.00
                };

                var url = await CreateProductAsync(product);
                Console.WriteLine($"Created at {url}");

                product = await GetProductAsync(url.PathAndQuery);
                ShowProduct(product);

                Console.WriteLine("Updating UnitPrice...");
                product.UnitPrice = (decimal?)80.81;
                await UpdateProductAsync(product);

                product = await GetProductAsync(url.PathAndQuery);
                ShowProduct(product);

                var statusCode = await DeleteProductAsync(product.ProductId.ToString());
                Console.WriteLine($"Deleted (HTTP Status = {(int)statusCode})");

                GetAllProductAsync();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}
