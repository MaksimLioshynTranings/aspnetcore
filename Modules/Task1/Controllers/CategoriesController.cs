﻿using DalForTasks.Models;
using Microsoft.AspNetCore.Mvc;

namespace Task1.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly NorthwindContext _context;

        public CategoriesController(NorthwindContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View(_context.Categories);
        }
    }
}
