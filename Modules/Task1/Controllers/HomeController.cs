﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using DalForTasks.Models;
using Task1.LoggerService;

namespace Task1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _log;
        private readonly ILoggerManager _logger;

        public HomeController(ILogger<HomeController> log, ILoggerManager logger)
        {
            _log = log;
            _logger = logger;
        }

        public string Logging()
        {
            try
            {
                throw new NullReferenceException();
            }
            catch (Exception e)
            {
                _log.LogInformation($"Message {e.Message} at {DateTime.Now}");
                _log.LogWarning($"Warning Message {e.Message} at {DateTime.Now}");
                _logger.LogInfo("Fetching all the Students from the storage");
            }

            return "Hello world!";
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
