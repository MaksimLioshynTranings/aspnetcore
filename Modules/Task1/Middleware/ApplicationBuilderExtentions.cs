﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.FileProviders;
using Microsoft.VisualStudio.Web.CodeGeneration.DotNet;

namespace Task1.Middleware
{
    public static class ApplicationBuilderExtentions
    {
        public static IApplicationBuilder UseNodeModule(this IApplicationBuilder app, string root)
        {
            string path = Path.Combine(root, "node_modules");
            PhysicalFileProvider fileProvider = new PhysicalFileProvider(path);
            var options = new StaticFileOptions
            {
                RequestPath = "/node_modules",
                FileProvider = fileProvider
            };

            app.UseStaticFiles(options);
            return app;
        }
    }
}
