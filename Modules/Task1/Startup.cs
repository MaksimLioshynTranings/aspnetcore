﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.IO;
using DalForTasks.Models;
using Task1.LoggerService;
using Task1.Middleware;

namespace Task1
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IConfiguration Config { get; }
        public ILogger<Startup> Logger { get; }

        public Startup(IConfiguration configuration, IConfiguration config, ILogger<Startup> logger)
        {
            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/bin/Debug/netcoreapp2.1/NLog.config"));
            Configuration = configuration;
            Config = config;
            Logger = logger;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            
            Logger.LogInformation("ConfigureServices called");
            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<NorthwindContext>(options => options.UseSqlServer(connection));
            
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSingleton<ILoggerManager, LoggerManager>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, 
                              IHostingEnvironment env,
                              IConfiguration config,
                              ILoggerFactory loggerFactory)
        {
            Logger.LogInformation("Configure called");
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddFile("Logs/Task1app-{Date}.txt");

            //new LoggerConfiguration()
            //    .MinimumLevel.Debug()
            //    .WriteTo.File("log.txt")
            //    .CreateLogger();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {

                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.ConfigureCustomExceptionMiddleware();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseNodeModule(env.ContentRootPath);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
           
        }
    }
}
