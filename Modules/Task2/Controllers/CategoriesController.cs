﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DalForTasks.Models;

namespace Task2.Controllers
{
    [FormatFilter]
    public class CategoriesController : Controller
    {
        private readonly NorthwindContext _context;

        public CategoriesController(NorthwindContext context)
        {
            _context = context;
        }

        #region create

        [ProducesResponseType(200)]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CategoryId,CategoryName,Description,Picture")]Categories categories, FormCollection data)
        {
            if (ModelState.IsValid)
            {
                _context.Add(categories);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(categories);
        }

        public IActionResult CreateCategory()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateCategory(CategoriesView category)
        {
            if (ModelState.IsValid)
            {

                Categories categories = new Categories
                {
                    CategoryId = category.CategoryId,
                    CategoryName = category.CategoryName,
                    Description = category.Description,
                    Picture = category.Picture = ConvertToByteImage(category.Image)
            };

                _context.Add(categories);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        #endregion

        #region read

        public async Task<IActionResult> Index()
        {
            return View(await _context.Categories.ToListAsync());
        }

        [Route("/images/{id:int:min(1)}")]
        public IActionResult ImagesForTest(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categories = _context.Categories.FirstOrDefault(m => m.CategoryId == id);
            if (categories == null)
            {
                return NotFound();
            }
            return View(categories.Picture);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categories = await _context.Categories
                .FirstOrDefaultAsync(m => m.CategoryId == id);
            if (categories == null)
            {
                return NotFound();
            }

            return View(categories);
        }

        [Produces("application/octet-stream")]
        public ActionResult<byte[]> ShowImageById(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var categories = _context.Categories.FirstOrDefault(m => m.CategoryId == id);
            if (categories == null)
            {
                return NotFound();
            }
            return View(categories.Picture);
        }

        #endregion

        #region update

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categories = await _context.Categories.FindAsync(id);
            if (categories == null)
            {
                return NotFound();
            }
            
            CategoriesView categoriesView = new CategoriesView
            {
                CategoryId = categories.CategoryId,
                CategoryName = categories.CategoryName,
                Description = categories.Description,
                Picture = categories.Picture
            };
            return View(categoriesView);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, CategoriesView category)
        {
            if (id != category.CategoryId)
            {
                return NotFound();
            }

            var categories = await _context.Categories.FindAsync(id);
            if (ModelState.IsValid)
            {
                try
                {
                    if (category.Image != null)
                    {
                        categories.Picture = ConvertToByteImage(category.Image);
                    }

                    categories.CategoryName = category.CategoryName;
                    categories.Description = category.Description;

                    _context.Update(categories);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CategoriesExists(categories.CategoryId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }
      
        #endregion

        #region delete

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categories = await _context.Categories
                .FirstOrDefaultAsync(m => m.CategoryId == id);
            if (categories == null)
            {
                return NotFound();
            }

            return View(categories);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var categories = await _context.Categories.FindAsync(id);
            _context.Categories.Remove(categories);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        #endregion
        
        #region Auximilary functions

        private byte[] ConvertToByteImage(IFormFile file)
        {
            byte[] imageData;

            using (var binaryReader = new BinaryReader(file.OpenReadStream()))
            {
                imageData = binaryReader.ReadBytes((int)file.Length);
            }

            return imageData;
        }

        private async Task<IActionResult> FixImages()
        {
            var categories = _context.Categories.ToList();
            foreach (Categories category in categories)
            {
                category.Picture = category.Picture.TakeLast(category.Picture.Length - 78).ToArray();
                _context.Categories.Update(category);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(nameof(Index));
        }

        private bool CategoriesExists(int id)
        {
            return _context.Categories.Any(e => e.CategoryId == id);
        }

        #endregion
    }
}
