﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using DalForTasks.Models;
using Task2.Interfaces;

namespace Task2.Controllers
{
    public class HomeController : Controller
    {
        private IMessageSender _messageSender;
        public HomeController(IMessageSender messageSender)
        {
            _messageSender = messageSender;
        }

        public IActionResult Index()
        {
            ViewData["Message"] = "Hello world!";
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            ViewData["Message"] = "Error.";
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
