﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using static System.String;

namespace Task2.Controllers
{
    public class UploadFilesController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Show(string str)
        {
            return View(System.IO.File.ReadAllBytes(str));
        }

        [HttpPost("UploadFiles")]
        public async Task<IActionResult> Post(List<IFormFile> files)
        {
            string folderPath = $"{Directory.GetCurrentDirectory()}\\MyStaticFiles";//Path.GetTempFileName();
            string filePath = Empty;
            foreach (IFormFile formFile in files)
            {
                if (formFile.Length > 0)
                {
                    filePath = $"{folderPath}\\{formFile.FileName}";
                    //using (var stream = new FileStream(filePath, FileMode.Create))
                    //{
                    //    await formFile.CopyToAsync(stream);
                    //}
                }
            }

            return RedirectToAction("Show", new{str = filePath });
        }
    }


}
