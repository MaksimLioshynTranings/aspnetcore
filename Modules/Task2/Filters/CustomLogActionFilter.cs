﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Task2.Filters
{
    public class CustomLogActionFilter : IActionFilter
    {
        private IConfiguration Config { get; }

        private readonly ILogger<CustomLogActionFilter> _log;

        public CustomLogActionFilter(ILogger<CustomLogActionFilter> log, IConfiguration config)
        {
            _log = log;
            Config = config;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            string controllerName = $"{context.RouteData.Values["controller"]}Controller";
            string actionName = $"{context.RouteData.Values["action"]}";
            var conf = ResultFilter(controllerName, actionName);
            var val = conf?.Value ?? "false";
            if (bool.Parse(val))
            {
                _log.LogInformation($"OnActionExecuting {controllerName} controller, {actionName} action starting...");
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            string controllerName = $"{context.RouteData.Values["controller"]}Controller";
            string actionName = $"{context.RouteData.Values["action"]}";
            var conf = ResultFilter(controllerName, actionName);
            var val = conf?.Value ?? "false";
            if (bool.Parse(val))
            {
                _log.LogInformation($"OnActionExecuting {controllerName} controller, {actionName} action completed.");
            }
        }

        private IConfigurationSection ResultFilter(string controllerName, string actionName)
        {
            var getSection = Config.GetSection("EnableLogging").GetSection(controllerName);
            IEnumerable<IConfigurationSection> getActionName = getSection.GetChildren().Any() ? getSection.GetChildren() : null;
            return getActionName != null ? getActionName.FirstOrDefault(u => u.Key == actionName) : getSection;
        }
    }
}
