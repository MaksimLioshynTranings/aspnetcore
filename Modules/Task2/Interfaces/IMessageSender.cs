﻿namespace Task2.Interfaces
{
    public interface IMessageSender
    {
        string Send();
    }
}