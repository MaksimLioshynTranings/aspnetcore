﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Task2.Middleware
{
    public class CacheMiddleware
    {
        protected RequestDelegate _next;

        public CacheMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (httpContext.Request.HasFormContentType)
            {
                IFormFile formFile = httpContext.Request.Form.Files.FirstOrDefault();
                string contentType = formFile?.ContentType;
                if (contentType != null && contentType.Contains("image/"))
                {
                    string folderPath = $"{Directory.GetCurrentDirectory()}\\MyStaticFiles";
                    string filePath = $@"{folderPath}\{formFile.FileName}";
                    if (File.Exists(filePath))
                    {
                        httpContext.Items.Add("imageCache", true);
                    }
                    else
                    {
                        if (formFile.Length > 0)
                        {
                            using (var stream = new FileStream(filePath, FileMode.Create))
                            {
                                await formFile.CopyToAsync(stream);
                            }
                        }
                    }
                }
            }

            await _next(httpContext);
        }
    }
}
