﻿using System;

namespace Task2.Service
{
    public class TimeService
    {
        public TimeService()
        {
            Time = DateTime.Now.ToString("hh:mm:ss");
        }
        public string Time { get; }
    }
}
