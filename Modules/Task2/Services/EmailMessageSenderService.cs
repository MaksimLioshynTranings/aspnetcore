﻿using Task2.Interfaces;

namespace Task2.Services
{
    public class EmailMessageSenderService : IMessageSender
    {
        public string Send()
        {
            return "Sent by Email";
        }
    }
}