﻿using DalForTasks.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using NLog;
using System;
using System.IO;
using System.Linq;
using Task2.Filters;
using Task2.Interfaces;
using Task2.LoggerService;
using Task2.Middleware;
using Task2.Service;
using Task2.Services;
using SameSiteMode = Microsoft.AspNetCore.Http.SameSiteMode;

namespace Task2
{
    public class Startup
    {
        public ILogger<Startup> Logger { get; }
        public IConfiguration Config { get; }
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IConfiguration config, ILogger<Startup> logger)
        {
            LogManager.LoadConfiguration($"{Directory.GetCurrentDirectory()}/bin/Debug/netcoreapp2.1/NLog.config");
            Configuration = configuration;
            Config = config;
            Logger = logger;
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<NorthwindContext>(options => options.UseSqlServer(connection));

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSingleton<ILoggerManager, LoggerManager>();
            services.AddTransient<IMessageSender, EmailMessageSenderService>();
            services.AddTransient<TimeService>();
            services.AddLogging(configure => configure.AddConfiguration(Configuration.GetSection("Logging")).AddDebug()).AddTransient<CustomLogActionFilter>();

            services.AddMvc(options =>
            {
                options.RespectBrowserAcceptHeader = true;
                options.OutputFormatters.RemoveType<TextOutputFormatter>();
                options.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
                options.Filters.Add<CustomLogActionFilter>();
            })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app,
                              IHostingEnvironment env,
                              ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddFile("Logs/Task2app-{Date}.txt");
            //Audit.Core.Configuration.Setup()
            //    .UseFileLogProvider(
            //        options => options.Directory($"{Directory.GetCurrentDirectory()}/Logs")
            //        );


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();

            app.UseMiddleware<CacheMiddleware>();

            app.Use(async (context, next) =>
            {
                if (context.Items.ContainsKey("imageCache"))
                {
                    context.Response.GetTypedHeaders().CacheControl =
                        new CacheControlHeaderValue
                        {
                            Public = true,
                            MaxAge = TimeSpan.FromSeconds(10)
                        };
                    context.Response.Headers[HeaderNames.Vary] =
                        new[] { "Accept-Encoding" };
                }

                await next();
            });

            //app.UseStaticFiles(new StaticFileOptions()
            //{
            //    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"MyStaticFiles")),
            //    RequestPath = new PathString("/StaticFiles"),
            //    OnPrepareResponse =
            //        r =>
            //        {
            //            string path = r.File.PhysicalPath;
            //            if (path.EndsWith(".css") || path.EndsWith(".js") || path.EndsWith(".gif") || path.EndsWith(".jpg") || path.EndsWith(".png") || path.EndsWith(".svg"))
            //            {
            //                TimeSpan maxAge = new TimeSpan(7, 0, 0, 0);
            //                r.Context.Response.Headers.Append("Cache-Control", "max-age=" + maxAge.TotalSeconds.ToString("0"));
            //            }
            //        }
            //});
            app.UseStaticFiles();

            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "images",
                    template: "/{images}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
