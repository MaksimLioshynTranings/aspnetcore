﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Task3.Helpers
{
    public static class CustomHtmlHelpers
    {
        public static IHtmlContent NorthwindImageLink(this IHtmlHelper html, int? id, string text)
        {
            return new HtmlFormattableString($"<a href='/images/{id}'>{text}</a>");
        }
        
    }
}
