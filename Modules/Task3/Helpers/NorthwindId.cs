﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Task3.Helpers
{
    [HtmlTargetElement("a", Attributes = IdAttributeName)]
    public class NorthwindId: TagHelper
    {
        private const string IdAttributeName = "northwind-id";

        [HtmlAttributeName(IdAttributeName)]
        public int Id { get; set; } = 0;

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.SetAttribute("href",$"images/{Id}");
        }
    }
}
