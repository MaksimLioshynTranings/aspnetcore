﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;

namespace Task3.ViewComponents
{
    public class Breadcrumbs : ViewComponent
    {
        public Breadcrumbs()
        {
        }

        public IHtmlContent Invoke()
        {
            var pathValueSplit = HttpContext.Request.Path.Value.Split('/');
            var controllerName = ViewContext.ActionDescriptor.RouteValues.FirstOrDefault(u => u.Key == "controller").Value;
            var actionName = ViewContext.ActionDescriptor.RouteValues.FirstOrDefault(u => u.Key == "action").Value;

            if (actionName == "Index")
            {
                return new HtmlContentBuilder();
            }

            int id = 0;
            if (pathValueSplit.Length == 4)
            {
                id = int.Parse(pathValueSplit.Last());
            }

            var breadcrumb = new HtmlContentBuilder();

            string breadcrumbControllerName = $"<a href='/{controllerName}/Index'>{controllerName}</a>";
            string breadcrumbActionName = $"> <a href='/{controllerName}/{actionName}'>{actionName}</a>";
            string breadcrumbId = id == 0 ? string.Empty : $"> <a href='/{controllerName}/{actionName}/{id}'>{id}</a>";

            return breadcrumb.AppendHtml($"{breadcrumbControllerName} {breadcrumbActionName} {breadcrumbId}");
        }
    }
}
