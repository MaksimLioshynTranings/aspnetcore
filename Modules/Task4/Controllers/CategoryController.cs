﻿using DalForTasks.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Task4.Controllers
{
    [Route("api")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly NorthwindContext _context;

        public CategoryController(NorthwindContext context)
        {
            _context = context;
        }

        #region CRUD

        [HttpGet]
        public ActionResult<List<Categories>> Get()
        {
            return _context.Categories.ToList();
        }

        [HttpGet("{id}", Name = "GetCategory")]
        public ActionResult<Categories> Get(int id)
        {
            return _context.Categories.FirstOrDefault(u => u.CategoryId == id);
        }

        [HttpPost]
        public IActionResult Create(Categories categories)
        {
            try
            {
                _context.Categories.Add(categories);
                _context.SaveChanges();
                return CreatedAtRoute("GetCategory", new { id = categories.CategoryId }, categories);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Categories category)
        {
            var updateCategory = _context.Categories.Find(id);
            if (updateCategory == null)
            {
                return NotFound();
            }

            updateCategory.CategoryName = category.CategoryName;
            updateCategory.Description = category.Description;

            try
            {
                _context.Categories.Update(updateCategory);
                _context.SaveChanges();
                return CreatedAtRoute("GetCategory", new { id = category.CategoryId }, updateCategory);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var category = _context.Categories.Find(id);
            if (category == null)
            {
                return NotFound();
            }

            try
            {
                _context.Categories.Remove(category);
                _context.SaveChanges();
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        #endregion

        [HttpGet]
        [Route("images/{id:int:min(1)}")]
        public ActionResult<Categories> GetImage(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categories = _context.Categories.FirstOrDefault(m => m.CategoryId == id);
            if (categories == null)
            {
                return NotFound();
            }
            return Ok(categories.Picture);
        }

        [HttpPut]
        [Route("UpdateImage/{id:int:min(1)}")]
        public IActionResult UpdateImage(int? id, Categories categoryModel)
        {
            var category = _context.Categories.Find(id);
            if (category == null)
            {
                return NotFound();
            }

            category.Picture = categoryModel.Picture;

            try
            {
                _context.Categories.Update(category);
                _context.SaveChanges();
                return CreatedAtRoute("GetCategory", new { id = category.CategoryId }, category);
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
