﻿using DalForTasks.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using NSwag.Annotations;

namespace Task4.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly NorthwindContext _context;

        public ProductsController(NorthwindContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Products>> Get()
        {
            return _context.Products.ToList();
        }

        [HttpGet("{id}", Name = "GetProduct")]
        public ActionResult<Products> Get(int id)
        {
            Products item = _context.Products.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        [SwaggerOperation(operationId: "CreateProducts")]
        public IActionResult Create(Products product)
        {
            _context.Products.Add(product);
            _context.SaveChanges();

            return CreatedAtRoute("GetProduct", new { id = product.ProductId }, product);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Products product)
        {
            var products = _context.Products.Find(id);
            if (products == null)
            {
                return NotFound();
            }

            products.ProductName = product.ProductName;
            products.QuantityPerUnit = product.QuantityPerUnit;
            products.UnitPrice = product.UnitPrice;

            _context.Products.Update(products);
            _context.SaveChanges();
            return CreatedAtRoute("GetProduct", new { id = product.ProductId }, product);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var product = _context.Products.Find(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Products.Remove(product);
            _context.SaveChanges();
            return Ok();
        }
    }
}
