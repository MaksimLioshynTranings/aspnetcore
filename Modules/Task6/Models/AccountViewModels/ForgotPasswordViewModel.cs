﻿using System.ComponentModel.DataAnnotations;

namespace Task6.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
