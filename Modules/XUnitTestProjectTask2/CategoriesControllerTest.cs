﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using DalForTasks.Models;
using Task2.Controllers;
using Xunit;

namespace XUnitTestProjectTask2
{
    public class CategoriesControllerTest
    {
        private readonly NorthwindContext _context;

        public CategoriesControllerTest()
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkSqlServer()
                .BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<NorthwindContext>();

            builder.UseSqlServer("Data Source=EPBYGOMW0751\\MSSQLSERVER1;Initial Catalog=Northwind;Integrated Security=True")
                .UseInternalServiceProvider(serviceProvider);

            _context = new NorthwindContext(builder.Options);
        }

        [Fact]
        public void IndexTest()
        {
            // Arrange
            var controller = new CategoriesController(_context);

            // Act
            var result = controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result.Result);
            var model = Assert.IsAssignableFrom<IEnumerable<Categories>>(
                viewResult.Model);
            Assert.Equal(_context.Categories.Count(), model.Count());
        }

        [Fact]
        public void DetailsTest()
        {
            // Arrange
            var controller = new CategoriesController(_context);

            // Act
            var id = _context.Categories.FirstOrDefault()?.CategoryId;
            var result = controller.Details(id);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result.Result);
            var model = Assert.IsAssignableFrom<Categories>(
                viewResult.Model);
            Assert.Equal(_context.Categories.FirstOrDefault()?.CategoryId, model.CategoryId);
        }
    }
}
