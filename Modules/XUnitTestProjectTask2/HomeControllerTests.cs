using Microsoft.AspNetCore.Mvc;
using Moq;
using Task2.Controllers;
using Task2.Interfaces;
using Xunit;

namespace XUnitTestProjectTask2
{
    public class HomeControllerTests
    {
        [Fact]
        public void IndexViewDataMessage()
        {
            // Arrange
            var mock = new Mock<IMessageSender>();
            HomeController controller = new HomeController(mock.Object);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            //Assert.Equal("Hello world!", result.ViewData["Message"]);
            Assert.Equal(viewResult.ContentType, result.ContentType);
        }

        [Fact]
        public void AboutViewDataMessage()
        {
            // Arrange
            var mock = new Mock<IMessageSender>();
            HomeController controller = new HomeController(mock.Object);

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal(viewResult.ViewData["Message"], result.ViewData["Message"]);
        }

        [Fact]
        public void ContactViewDataMessage()
        {
            // Arrange
            var mock = new Mock<IMessageSender>();
            HomeController controller = new HomeController(mock.Object);

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal(viewResult.ViewData["Message"], result.ViewData["Message"]);
        }

        [Fact]
        public void PrivacyViewDataMessage()
        {
            // Arrange
            var mock = new Mock<IMessageSender>();
            HomeController controller = new HomeController(mock.Object);

            // Act
            ViewResult result = controller.Privacy() as ViewResult;

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal(viewResult.ViewData["Message"], result.ViewData["Message"]);
        }

    }
}
