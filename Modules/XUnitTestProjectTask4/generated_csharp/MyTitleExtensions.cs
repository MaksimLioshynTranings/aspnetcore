// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace Task4.Controllers
{
    using Models;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Extension methods for MyTitle.
    /// </summary>
    public static partial class MyTitleExtensions
    {
            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='product'>
            /// </param>
            public static Stream CreateProducts(this IMyTitle operations, Products product)
            {
                return operations.CreateProductsAsync(product).GetAwaiter().GetResult();
            }

            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='product'>
            /// </param>
            /// <param name='cancellationToken'>
            /// The cancellation token.
            /// </param>
            public static async Task<Stream> CreateProductsAsync(this IMyTitle operations, Products product, CancellationToken cancellationToken = default(CancellationToken))
            {
                var _result = await operations.CreateProductsWithHttpMessagesAsync(product, null, cancellationToken).ConfigureAwait(false);
                _result.Request.Dispose();
                return _result.Body;
            }

    }
}
